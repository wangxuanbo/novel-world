package com.novel.common.dao.impl;

import com.novel.common.dao.DemoDao;
import com.novel.common.entity.Demo;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
public class DemoDaoImpl extends BaseDaoImpl<Demo> implements DemoDao {
}
