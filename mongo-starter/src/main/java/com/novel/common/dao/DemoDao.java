package com.novel.common.dao;

import com.novel.common.entity.Demo;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
public interface DemoDao extends BaseDao<Demo> {
}
