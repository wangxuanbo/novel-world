package com.novel.common.entity;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@Document(collection = "T_Demo")
public class Demo extends Entity {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Demo{" +
                "name='" + name + '\'' +
                "} " + super.toString();
    }

}
