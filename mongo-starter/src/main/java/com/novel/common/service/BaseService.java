package com.novel.common.service;

import com.novel.common.entity.Entity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
public interface BaseService<T extends Entity> {

    /**
     * 分页查询
     *
     * @param pageable 分页信息
     * @return Page
     */
    Page<T> page(Pageable pageable);

    /**
     * 分页查询
     *
     * @param entity 实体
     * @param pageable 分页信息
     * @return Page
     */
    Page<T> page(T entity, Pageable pageable);

    /**
     * 根据id查询
     *
     * @param id 主键
     * @return T
     */
    T findById(String id);

    /**
     * 根据ids查询
     *
     * @param ids 主键
     * @return List<T>
     */
    List<T> findByIds(List<String> ids);

    /**
     * 查询所有
     *
     * @return List<T>
     */
    List<T> findAll();

    /**
     * 查询
     *
     * @param entity 实体
     * @return List<T>
     */
    List<T> find(T entity);

    /**
     * 新增
     *
     * @param entity 实体
     */
    void add(T entity);

    /**
     * 批量新增
     *
     * @param entities 实体
     */
    void batchAdd(List<T> entities);

    /**
     * 根据id更新，更新不为null的字段
     *
     * @param entity 实体
     */
    void modify(T entity);

    /**
     * 根据id删除
     *
     * @param id 主键
     */
    void deleteById(String id);

}
