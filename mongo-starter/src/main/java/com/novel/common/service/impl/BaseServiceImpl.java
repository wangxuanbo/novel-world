package com.novel.common.service.impl;

import com.novel.common.dao.BaseDao;
import com.novel.common.entity.Entity;
import com.novel.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
public abstract class BaseServiceImpl<T extends Entity> implements BaseService<T> {

    @Autowired
    private BaseDao<T> baseDao;

    @Override
    public Page<T> page(Pageable pageable) {
        return baseDao.page(new Criteria(), pageable);
    }

    @Override
    public Page<T> page(T entity, Pageable pageable) {
        Example<T> example = Example.of(entity);
        Criteria criteria = Criteria.byExample(example);
        return baseDao.page(criteria, pageable);
    }

    @Override
    public T findById(String id) {
        return baseDao.findById(id);
    }

    @Override
    public List<T> findByIds(List<String> ids) {
        return baseDao.findByIds(ids);
    }

    @Override
    public List<T> findAll() {
        return baseDao.findAll();
    }

    @Override
    public List<T> find(T entity) {
        return baseDao.find(entity);
    }

    @Override
    public void add(T entity) {
        baseDao.insert(entity);
    }

    @Override
    public void batchAdd(List<T> entities) {
        baseDao.batchInsert(entities);
    }

    @Override
    public void modify(T entity) {
        baseDao.updateNotNullById(entity);
    }

    @Override
    public void deleteById(String id) {
        baseDao.deleteById(id);
    }
}
