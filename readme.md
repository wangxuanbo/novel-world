# novel-world

> 小说世界

## 项目介绍

基于webmagic爬取小说，提供在线小说阅读

## 软件架构

* webmagic爬取小说
* Spring Boot搭建基础服务
* Mongodb存储数据

## 安装教程

* 修改配置: `novel-world/crawl-bxwx9/src/main/resources/application.yml`
    ```yaml
    server:
      # 端口号
      port: 9091
    
    spring:
      data:
        mongodb:
          # mongodb地址
          uri: mongodb://${username}:${password}@${ip}:${port}/${database}
    ```
* 安装到本地(**novel-world目录下**)
    ```shell
    mvn clean install -DskipTests
    ```
* 运行项目(**novel-world目录下**)
    ```shell
    java -jar crawl-bxwx9/target/crawl-bxwx9-1.0.jar
    ```

## 使用

* 程序运行1分钟后，会将笔下文学的所有书籍（不包含章节、内容）抓取过来，来源(source字段)标记为`bxwx9`，以后每周更新一次书籍
    ```json
    {
        "_id" : ObjectId("5bdc141f135eb4119cf92c25"),
        "name" : "大宋超级学霸",
        "description" : "    那一年，大宋甲级足球联赛正热，国民老公苏东坡金榜高中。那一年，京城房价一飞冲天，老干部欧阳修买房不及时被夫人赶出家门。    就在那一年，赵官家上元夜偷窥香艳女相扑，被朝阳群众司马光当场抓获。    也是那一年，王老虎携女参加非诚勿扰，扬言非进士不嫁，金明池畔四大才子仓惶奔逃。    还是那一年，河东狮喜拜婚堂，胭脂虎相亲正忙，全国神童大赛各路少年英才开始隆重登场。",
        "author" : "高月",
        "source" : "bxwx9",
        "url" : "https://www.bxwx9.org/b/36/36301/index.html",
        "image" : "https://www.bxwx9.org/image/36/36301/36301s.jpg",
        "type" : "历史军事",
        "status" : "连载中",
        "createAt" : ISODate("2018-11-02T09:08:47.446Z"),
        "updateAt" : ISODate("2018-11-03T02:21:21.408Z")
    }
    ```
* 程序运行5分钟后，会将来源(source字段)为`me`的书籍章节和内容抓取过来（主要是为了防止内容太多，云服务器磁盘不够）
* 将喜欢的书籍来源(source字段)修改为`me`即可。