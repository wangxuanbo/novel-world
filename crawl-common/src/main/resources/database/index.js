/**
 * mongodb索引
 */
db.getCollection('T_Book').createIndex({name: 1});
db.getCollection('T_Book').createIndex({author: 1});
db.getCollection('T_Book').createIndex({url: 1}, {unique: true});

db.getCollection('T_Chapter').createIndex({url: 1}, {unique: true});
db.getCollection('T_Chapter').createIndex({bookId: 1});

db.getCollection('T_Content').createIndex({chapterId: 1}, {unique: true});

db.getCollection('T_Recommendation').createIndex({bookId: 1}, {unique: true});
db.getCollection('T_Recommendation').createIndex({value: 1});