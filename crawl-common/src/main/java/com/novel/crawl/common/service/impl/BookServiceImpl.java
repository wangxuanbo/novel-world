package com.novel.crawl.common.service.impl;

import com.novel.common.service.impl.BaseServiceImpl;
import com.novel.crawl.common.dao.BookDao;
import com.novel.crawl.common.entity.Book;
import com.novel.crawl.common.service.BookService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@Service
public class BookServiceImpl extends BaseServiceImpl<Book> implements BookService {

    @Autowired
    private BookDao bookDao;

    @Override
    public List<Book> findBatchByBookIdStartAndSource(String bookId, String source, int size) {
        return bookDao.findBatchByBookIdStartAndSource(bookId, source, size);
    }

    @Override
    public Book findByUrl(String url) {
        return bookDao.findByUrl(url);
    }

    @Override
    public Page<Book> search(String word, String type, Pageable pageable) {
        // 类型存在
        if (StringUtils.isNotEmpty(type)) {
            Book book = new Book();
            book.setType(type);
            return super.page(book, pageable);
        }
        // 不存在类型根据name查询
        if (StringUtils.isEmpty(word)) {
            return super.page(pageable);
        }
        // 否则查询所有
        return bookDao.search(word, pageable);
    }

    @Override
    public void incrementView(String bookId) {
        bookDao.incrementView(bookId);
    }

}
