package com.novel.crawl.common.service;

import com.novel.common.service.BaseService;
import com.novel.crawl.common.entity.Content;

/**
 * @author zhilong.deng@hand-china.com
 * @date 2018/10/24
 * @version 1.0
 */
public interface ContentService extends BaseService<Content> {

    /**
     * 根据章节id查询内容
     *
     * @param chapterId 章节id
     * @return Content
     */
    Content findByChapterId(String chapterId);

    /**
     * 查询下一章节内容
     *
     * @param bookId 书籍id
     * @param currentChapterId 当前章节id
     * @return Content
     */
    Content findNextByBookIdAndChapterId(String bookId, String currentChapterId);

    /**
     * 查询上一章节内容
     *
     * @param bookId 书籍id
     * @param currentChapterId 当前章节id
     * @return Content
     */
    Content findPrevByBookIdAndChapterId(String bookId, String currentChapterId);
}
