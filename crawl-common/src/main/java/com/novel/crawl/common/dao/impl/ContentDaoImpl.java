package com.novel.crawl.common.dao.impl;

import com.novel.common.dao.impl.BaseDaoImpl;
import com.novel.crawl.common.dao.ContentDao;
import com.novel.crawl.common.entity.Content;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

/**
 * @author zhilong.deng@hand-china.com
 * @date 2018/10/24
 * @version 1.0
 */
@Repository
public class ContentDaoImpl extends BaseDaoImpl<Content> implements ContentDao {

    @Override
    public Content findByChapterId(String chapterId) {
        return findOne(Query.query(Criteria.where("chapterId").is(chapterId)));
    }

}
