package com.novel.crawl.common.service.impl;

import com.novel.common.service.impl.BaseServiceImpl;
import com.novel.crawl.common.dao.ChapterDao;
import com.novel.crawl.common.dao.ContentDao;
import com.novel.crawl.common.entity.Chapter;
import com.novel.crawl.common.entity.Content;
import com.novel.crawl.common.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zhilong.deng@hand-china.com
 * @date 2018/10/24
 * @version 1.0
 */
@Service
public class ContentServiceImpl extends BaseServiceImpl<Content> implements ContentService {

    @Autowired
    private ChapterDao chapterDao;

    @Autowired
    private ContentDao contentDao;

    @Override
    public Content findByChapterId(String chapterId) {
        return contentDao.findByChapterId(chapterId);
    }

    @Override
    public Content findNextByBookIdAndChapterId(String bookId, String currentChapterId) {
        Chapter chapter = chapterDao.findNextByBookIdAndChapterId(bookId, currentChapterId);
        if (chapter == null) {
            return null;
        }
        return contentDao.findByChapterId(chapter.getId());
    }

    @Override
    public Content findPrevByBookIdAndChapterId(String bookId, String currentChapterId) {
        Chapter chapter = chapterDao.findPrevByBookIdAndChapterId(bookId, currentChapterId);
        if (chapter == null) {
            return null;
        }
        return contentDao.findByChapterId(chapter.getId());
    }
}
