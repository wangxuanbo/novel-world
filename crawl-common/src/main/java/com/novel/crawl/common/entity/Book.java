package com.novel.crawl.common.entity;

import com.novel.common.entity.Entity;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@Document(collection = "T_Book")
public class Book extends Entity {

    public static final String NAME_KEY = "name";
    public static final String SOURCE_KEY = "source";
    public static final String URL_KEY = "url";
    public static final String VIEW_KEY = "view";

    public static final String SOURCE_TYPE_ME = "me";
    public static final String SOURCE_TYPE_BXWX = "bxwx9";

    /**
     * 名称
     */
    @NotBlank
    private String name;

    /**
     * 描述简介
     */
    private String description;

    /**
     * 作者
     */
    @NotBlank
    private String author;

    /**
     * 来源
     */
    @NotBlank
    private String source;

    /**
     * 书籍章节展示页
     */
    @NotBlank
    private String url;

    /**
     * 封面
     */
    private String image;

    /**
     * 书籍类型
     */
    @NotBlank
    private String type;

    /**
     * 连载状态
     */
    private String status;

    /**
     * 最新章节id
     */
    private String newChapterId;

    /**
     * 最新章节名
     */
    private String newChapterName;

    /**
     * 访问量
     */
    private Long view;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNewChapterId() {
        return newChapterId;
    }

    public void setNewChapterId(String newChapterId) {
        this.newChapterId = newChapterId;
    }

    public String getNewChapterName() {
        return newChapterName;
    }

    public void setNewChapterName(String newChapterName) {
        this.newChapterName = newChapterName;
    }

    public Long getView() {
        return view;
    }

    public void setView(Long view) {
        this.view = view;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", source='" + source + '\'' +
                ", url='" + url + '\'' +
                ", image='" + image + '\'' +
                ", type='" + type + '\'' +
                ", status='" + status + '\'' +
                ", newChapterId='" + newChapterId + '\'' +
                ", newChapterName='" + newChapterName + '\'' +
                ", view=" + view +
                "} " + super.toString();
    }
}
