package com.novel.crawl.common.controller;

import com.novel.crawl.common.entity.Book;
import com.novel.crawl.common.entity.Chapter;
import com.novel.crawl.common.service.BookService;
import com.novel.crawl.common.service.ChapterService;
import com.novel.crawl.common.util.Rest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@Validated
@RestController
@RequestMapping("/book")
public class BookController extends BaseController<Book> {

    @Autowired
    private BookService bookService;

    @Autowired
    private ChapterService chapterService;

    @GetMapping("/search")
    public Rest<Page<Book>> search(@RequestParam(required = false) String word,
                                   @RequestParam(required = false) String type,
                                   @PageableDefault(size = DEFAULT_SIZE) Pageable pageable) {
        return Rest.ok(bookService.search(word, type, pageable));
    }

    /**
     * 查询章节
     *
     * @param bookId 书籍id
     * @return Rest<List<T>>
     */
    @GetMapping("/{bookId}/chapter")
    public Rest<List<Chapter>> chapters(@PathVariable String bookId) {
        return Rest.ok(chapterService.findByBookId(bookId));
    }

}
