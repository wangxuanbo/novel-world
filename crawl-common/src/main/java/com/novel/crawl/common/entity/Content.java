package com.novel.crawl.common.entity;

import com.novel.common.entity.Entity;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author zhilong.deng@hand-china.com
 * @date 2018/10/24
 * @version 1.0
 */
@Document(collection = "T_Content")
public class Content extends Entity {

    @NotBlank
    private String chapterId;

    @NotBlank
    private String content;

    public Content() {
    }

    public Content(String chapterId, String content) {
        this.chapterId = chapterId;
        this.content = content;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Content{" +
                "chapterId='" + chapterId + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
