package com.novel.crawl.common.conf;

import com.novel.crawl.common.filter.ViewFilter;
import com.novel.crawl.common.service.BookService;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Web配置
 *
 * @author 奔波儿灞
 * @since 1.0
 */
@Configuration
public class WebConfiguration {

    /**
     * 配置浏览量过滤器
     *
     * @param bookService BookService
     * @return FilterRegistrationBean
     */
    @Bean
    public FilterRegistrationBean viewFilter(BookService bookService) {
        ViewFilter viewFilter = new ViewFilter(bookService);
        FilterRegistrationBean registration = new FilterRegistrationBean(viewFilter);
        registration.addUrlPatterns(ViewFilter.FILTER_CHAPTER_PATTERN, ViewFilter.FILTER_CONTENT_PATTERN);
        return registration;
    }

}
