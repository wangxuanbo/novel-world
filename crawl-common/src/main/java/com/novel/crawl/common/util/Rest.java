package com.novel.crawl.common.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Rest<T> implements Serializable {

    /**
     * 状态码
     */
    private int code;

    /**
     * 描述
     */
    private String message;

    /**
     * 数据
     */
    private T data;

    private Rest() {
    }

    private Rest(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> Rest<T> ok() {
        return new Rest<>(HttpStatus.OK.value(), null, null);
    }

    public static <T> Rest<T> ok(String message) {
        return new Rest<>(HttpStatus.OK.value(), message, null);
    }

    public static <T> Rest<T> ok(T data) {
        return new Rest<>(HttpStatus.OK.value(), null, data);
    }

    public static <T> Rest<T> ok(String message, T data) {
        return new Rest<>(HttpStatus.OK.value(), message, data);
    }

    public static <T> Rest<T> fail(HttpStatus status, String message) {
        return new Rest<>(status.value(), message, null);
    }

    public static <T> Rest<T> fail(int code, String message) {
        return new Rest<>(code, message, null);
    }

    public static <T> Rest<T> fail(HttpStatus status, String message, T data) {
        return new Rest<>(status.value(), message, data);
    }

    public static <T> Rest<T> fail(int code, String message, T data) {
        return new Rest<>(code, message, data);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }

}
