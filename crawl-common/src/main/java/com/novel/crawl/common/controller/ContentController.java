package com.novel.crawl.common.controller;

import com.novel.crawl.common.entity.Content;
import com.novel.crawl.common.service.ContentService;
import com.novel.crawl.common.util.Rest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhilong.deng@hand-china.com
 * @date 2018/10/24
 * @version 1.0
 */
@Validated
@RestController
@RequestMapping("/content")
public class ContentController extends BaseController<Content> {

    @Autowired
    private ContentService contentService;

    /**
     * 查询下一章节内容
     *
     * @param bookId 书籍id
     * @param currentChapterId 当前章节id
     * @return Rest<Content>
     */
    @GetMapping("/next")
    public Rest<Content> nextContent(@RequestParam String bookId, @RequestParam String currentChapterId) {
        return Rest.ok(contentService.findNextByBookIdAndChapterId(bookId, currentChapterId));
    }

    /**
     * 查询上一章节内容
     *
     * @param bookId 书籍id
     * @param currentChapterId 当前章节id
     * @return Rest<Content>
     */
    @GetMapping("/prev")
    public Rest<Content> prevContent(@RequestParam String bookId, @RequestParam String currentChapterId) {
        return Rest.ok(contentService.findPrevByBookIdAndChapterId(bookId, currentChapterId));
    }

}
