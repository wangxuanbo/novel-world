package com.novel.crawl.common.dao;

import com.novel.common.dao.BaseDao;
import com.novel.crawl.common.entity.Chapter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author zhilong.deng@hand-china.com
 * @date 2018/10/24
 * @version 1.0
 */
public interface ChapterDao extends BaseDao<Chapter> {

    /**
     * 根据章节url查询章节
     *
     * @param url 章节url
     * @return 章节
     */
    Chapter findByUrl(String url);

    /**
     * 根据章节urls查询章节
     *
     * @param urls 章节urls
     * @return 章节
     */
    List<Chapter> findByUrls(List<String> urls);

    /**
     * 根据bookId，分页查询
     *
     * @param bookId 书籍id
     * @param pageable 分页信息
     * @return Page<Chapter>
     */
    Page<Chapter> pageByBookId(String bookId, Pageable pageable);

    /**
     * 根据bookId查询
     *
     * @param bookId 书籍id
     * @return List<Chapter>
     */
    List<Chapter> findByBookId(String bookId);

    /**
     * 查询下一章节
     *
     * @param bookId 书籍id
     * @param currentChapterId 当前章节id
     * @return Chapter
     */
    Chapter findNextByBookIdAndChapterId(String bookId, String currentChapterId);

    /**
     * 查询上一章节
     *
     * @param bookId 书籍id
     * @param currentChapterId 当前章节id
     * @return Chapter
     */
    Chapter findPrevByBookIdAndChapterId(String bookId, String currentChapterId);

}
