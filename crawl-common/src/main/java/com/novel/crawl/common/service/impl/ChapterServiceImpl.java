package com.novel.crawl.common.service.impl;

import com.novel.common.service.impl.BaseServiceImpl;
import com.novel.crawl.common.dao.ChapterDao;
import com.novel.crawl.common.entity.Chapter;
import com.novel.crawl.common.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhilong.deng@hand-china.com
 * @version 1.0
 * @date 2018/10/24
 */
@Service
public class ChapterServiceImpl extends BaseServiceImpl<Chapter> implements ChapterService {

    @Autowired
    private ChapterDao chapterDao;

    @Override
    public Chapter findByUrl(String url) {
        return chapterDao.findByUrl(url);
    }

    @Override
    public List<Chapter> findByUrls(List<String> urls) {
        return chapterDao.findByUrls(urls);
    }

    @Override
    public Page<Chapter> pageByBookId(String bookId, Pageable pageable) {
        return chapterDao.pageByBookId(bookId, pageable);
    }

    @Override
    public List<Chapter> findByBookId(String bookId) {
        return chapterDao.findByBookId(bookId);
    }
}
