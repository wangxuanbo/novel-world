package com.novel.crawl.common.dao.impl;

import com.novel.common.dao.impl.BaseDaoImpl;
import com.novel.crawl.common.dao.RecommendationDao;
import com.novel.crawl.common.entity.Recommendation;
import org.springframework.stereotype.Repository;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@Repository
public class RecommendationDaoImpl extends BaseDaoImpl<Recommendation> implements RecommendationDao {
}
