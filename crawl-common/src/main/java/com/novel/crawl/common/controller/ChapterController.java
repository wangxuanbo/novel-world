package com.novel.crawl.common.controller;

import com.novel.crawl.common.entity.Chapter;
import com.novel.crawl.common.entity.Content;
import com.novel.crawl.common.service.ContentService;
import com.novel.crawl.common.util.Rest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhilong.deng@hand-china.com
 * @date 2018/10/24
 * @version 1.0
 */
@Validated
@RestController
@RequestMapping("/chapter")
public class ChapterController extends BaseController<Chapter> {

    @Autowired
    private ContentService contentService;

    /**
     * 查询章节内容
     *
     * @param chapterId 章节id
     * @return Rest<Content>
     */
    @GetMapping("/{chapterId}/content")
    public Rest<Content> content(@PathVariable String chapterId) {
        return Rest.ok(contentService.findByChapterId(chapterId));
    }

}
