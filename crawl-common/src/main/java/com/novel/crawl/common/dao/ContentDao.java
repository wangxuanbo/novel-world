package com.novel.crawl.common.dao;

import com.novel.common.dao.BaseDao;
import com.novel.crawl.common.entity.Content;

/**
 * @author zhilong.deng@hand-china.com
 * @date 2018/10/24
 * @version 1.0
 */
public interface ContentDao extends BaseDao<Content> {

    /**
     * 根据章节id查询内容
     *
     * @param chapterId 章节id
     * @return Content
     */
    Content findByChapterId(String chapterId);
}
