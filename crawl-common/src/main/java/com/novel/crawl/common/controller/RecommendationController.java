package com.novel.crawl.common.controller;

import com.novel.crawl.common.entity.Recommendation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@Validated
@RestController
@RequestMapping("/recommendation")
public class RecommendationController extends BaseController<Recommendation> {
}
