package com.novel.crawl.common.entity;

import com.novel.common.entity.Entity;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@Document(collection = "T_Recommendation")
public class Recommendation extends Entity {

    /**
     * 书籍id
     */
    private String bookId;

    /**
     * 推荐理由
     */
    private String reason;

    /**
     * 推荐指数
     */
    private Integer value;

    @Transient
    private Book book;

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "Recommendation{" +
                "bookId='" + bookId + '\'' +
                ", reason='" + reason + '\'' +
                ", value=" + value +
                ", book=" + book +
                "} " + super.toString();
    }
}
