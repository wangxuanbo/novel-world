package com.novel.crawl.common.entity;

import com.novel.common.entity.Entity;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;

/**
 * @author zhilong.deng@hand-china.com
 * @date 2018/10/24
 * @version 1.0
 */
@Document(collection = "T_Chapter")
public class Chapter  extends Entity {

    public static final String BOOK_ID_KEY = "bookId";
    public static final String URL_KEY = "url";
    public static final String SORTED_VALUE_KEY = "sortedValue";

    /**
     * 书籍id
     */
    @NotBlank
    private String bookId;

    /**
     * 章节标题
     */
    @NotBlank
    private String title;

    /**
     * 章节绝对路径
     */
    @NotBlank
    private String url;

    /**
     * 相对路径
     */
    @NotBlank
    private String relativeUrl;

    /**
     * 排序权重
     */
    @Min(1)
    private Long sortedValue;

    public Chapter() {
    }

    public Chapter(String bookId, String title, String url, String relativeUrl, Long sortedValue) {
        this.bookId = bookId;
        this.title = title;
        this.url = url;
        this.relativeUrl = relativeUrl;
        this.sortedValue = sortedValue;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRelativeUrl() {
        return relativeUrl;
    }

    public void setRelativeUrl(String relativeUrl) {
        this.relativeUrl = relativeUrl;
    }

    public Long getSortedValue() {
        return sortedValue;
    }

    public void setSortedValue(Long sortedValue) {
        this.sortedValue = sortedValue;
    }

    @Override
    public String toString() {
        return "Chapter{" +
                "bookId='" + bookId + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", relativeUrl='" + relativeUrl + '\'' +
                ", sortedValue=" + sortedValue +
                "} " + super.toString();
    }
}
