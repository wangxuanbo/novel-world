package com.novel.crawl.common.service;

import com.novel.common.service.BaseService;
import com.novel.crawl.common.entity.Recommendation;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
public interface RecommendationService extends BaseService<Recommendation> {
}
