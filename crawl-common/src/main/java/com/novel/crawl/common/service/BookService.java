package com.novel.crawl.common.service;

import com.novel.common.service.BaseService;
import com.novel.crawl.common.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
public interface BookService extends BaseService<Book> {

    /**
     * 根据给定的bookId为起始查询一批数据
     *
     * @param bookId 起始的书籍id
     * @param source 源
     * @param size 一批的数据量
     * @return List<Book>
     */
    List<Book> findBatchByBookIdStartAndSource(String bookId, String source, int size);

    /**
     * 根据url查询书籍
     *
     * @param url 书籍url
     * @return Book
     */
    Book findByUrl(String url);

    /**
     * 搜索
     *
     * @param word 关键字
     * @param type 类型
     * @param pageable 分页信息
     * @return Page<Book>
     */
    Page<Book> search(String word, String type, Pageable pageable);

    /**
     * 访问量自增
     *
     * @param bookId 书籍id
     */
    void incrementView(String bookId);
}
