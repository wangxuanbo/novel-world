package com.novel.crawl.bxwx9.task;

import com.novel.crawl.bxwx9.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class CrawlTaskTest {

    @Autowired
    private CrawlTask crawlTask;

    @Test
    public void crawl() {
        crawlTask.crawl();
    }

    @Test
    public void crawlBook() {
        crawlTask.crawlBook();
    }

}
