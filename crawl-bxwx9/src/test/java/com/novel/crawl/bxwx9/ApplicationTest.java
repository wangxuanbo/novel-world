package com.novel.crawl.bxwx9;

import com.novel.crawl.common.dao.BookDao;
import com.novel.crawl.common.entity.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class ApplicationTest {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationTest.class);

    @Autowired
    private BookDao bookDao;

    @Test
    public void insert() {
        Book book = new Book();
        book.setName("demo1");
        bookDao.insert(book);
    }

    @Test
    public void batchInsert() {
        Book book1 = new Book();
        book1.setName("demo1");
        Book book2 = new Book();
        book2.setName("demo2");
        List<Book> books = Arrays.asList(book1, book2);
        bookDao.batchInsert(books);
        LOG.info("books: {}", books);
    }

    @Test
    public void find() {
        List<Book> books = bookDao.find(Query.query(Criteria.where("name").is("demo1")));
        LOG.info("books: {}", books);
    }

}
