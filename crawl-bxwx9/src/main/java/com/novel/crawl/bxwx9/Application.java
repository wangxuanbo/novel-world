package com.novel.crawl.bxwx9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
@SpringBootApplication(scanBasePackages = {
        "com.novel.crawl.common",
        "com.novel.crawl.bxwx9"
})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
