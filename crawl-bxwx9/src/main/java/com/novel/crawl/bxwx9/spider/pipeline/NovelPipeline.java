package com.novel.crawl.bxwx9.spider.pipeline;

import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 * Pipeline
 *
 * @author 奔波儿灞
 * @since 1.0
 */
@Component
public class NovelPipeline implements Pipeline {

    @Override
    public void process(ResultItems resultItems, Task task) {

    }

}
