package com.novel.crawl.bxwx9.spider;

import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/**
 * 退出爬虫后不销毁资源，供重复利用
 *
 * @author 奔波儿灞
 * @since 1.0
 */
public class BatterSpider extends Spider {

    /**
     * 组件是否初始化过
     */
    private boolean init;

    /**
     * create a spider with pageProcessor.
     *
     * @param pageProcessor pageProcessor
     */
    private BatterSpider(PageProcessor pageProcessor) {
        super(pageProcessor);
    }

    /**
     * create a spider with pageProcessor.
     *
     * @param pageProcessor pageProcessor
     * @return new spider
     * @see PageProcessor
     */
    public static Spider create(PageProcessor pageProcessor) {
        return new BatterSpider(pageProcessor);
    }

    /**
     * 保证初始化一次组件
     */
    @Override
    protected void initComponent() {
        if (init) {
            // 初始化过只添加url到scheduler
            if (startRequests != null) {
                for (Request request : startRequests) {
                    addRequest(request);
                }
                startRequests.clear();
            }
        } else {
            super.initComponent();
            init = true;
        }
    }

    /**
     * 运行完后不销毁组件
     */
    @Override
    public void close() {
    }
}
