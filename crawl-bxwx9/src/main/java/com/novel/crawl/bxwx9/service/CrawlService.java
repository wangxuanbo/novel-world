package com.novel.crawl.bxwx9.service;

/**
 * @author 奔波儿灞
 * @since 1.0
 */
public interface CrawlService {

    /**
     * 开始爬取
     */
    void crawl();

    void crawlBook();
}
