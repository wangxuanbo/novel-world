package com.novel.crawl.bxwx9.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 爬虫配置
 *
 * @author 奔波儿灞
 * @since 1.0
 */
@ConfigurationProperties(BookSpiderProperties.PREFIX)
public class BookSpiderProperties {

    public static final String PREFIX = "bookSpider";

    /**
     * 线程数
     */
    private int poolSize = 20;

    /**
     * 线程空闲时间，单位秒
     */
    private int keepAliveSeconds = 60;

    /**
     * 线程前缀
     */
    private String threadNamePrefix = PREFIX;

    public int getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public int getKeepAliveSeconds() {
        return keepAliveSeconds;
    }

    public void setKeepAliveSeconds(int keepAliveSeconds) {
        this.keepAliveSeconds = keepAliveSeconds;
    }

    public String getThreadNamePrefix() {
        return threadNamePrefix;
    }

    public void setThreadNamePrefix(String threadNamePrefix) {
        this.threadNamePrefix = threadNamePrefix;
    }
}
